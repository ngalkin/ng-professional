(function(ng) {

  ng.module('app')
    .directive('card', CardDirective);

  function CardController() {}

  CardController.prototype.remove = function () {
    this.CardListController.remove(this.item);
  };

  function CardDirective() {
    return {
      restrict: 'E',
      controller: CardController,
      controllerAs: '$ctrl',
      templateUrl: 'js/card/template.html',
      bindToController: true,
      scope: {
        item: '='
      },
      require: {
        CardListController: '^^cardList'
      }
    }
  }
})(angular)
