(function(ng) {

  ng.module('app')
    .service('CardService', CardService)
    .factory('CardFactory', CardFactory);

  var cards = [
    {
      id: 1,
      title: 'Card 1',
      todos: [
        { title: 'Todo 11', isComplete: false },
        { title: 'Todo 12', isComplete: false },
        { title: 'Todo 13', isComplete: false },
        { title: 'Todo 14', isComplete: false },
        { title: 'Todo 15', isComplete: false },
      ]
    },
    {
      id: 2,
      title: 'Card 2',
      todos: [
        { title: 'Todo 21', isComplete: false },
        { title: 'Todo 22', isComplete: false },
        { title: 'Todo 23', isComplete: false },
        { title: 'Todo 24', isComplete: true },
        { title: 'Todo 25', isComplete: false },
      ]
    },
    {
      id: 3,
      title: 'Card 3',
      todos: [
        { title: 'Todo 31', isComplete: true },
        { title: 'Todo 32', isComplete: false },
        { title: 'Todo 33', isComplete: true },
        { title: 'Todo 34', isComplete: true },
        { title: 'Todo 35', isComplete: false },
      ]
    }
  ];

  function CardService() {
    this.items = cards;
  }

  CardService.prototype.list = function() {
    return this.items;
  };

  CardService.prototype.detail = function (id) {
    return this.items.find(function(item) {
      return item.id === id;
    });
  };

  CardService.prototype.remove = function (card) {
    var index = this.items.findIndex(function(item) {
      return item.id === card.id;
    });
    this.items.splice(index, 1);
  };

  CardService.prototype.add = function(card) {
    card.id = this.cards.length;
    card.created = new Date();
    this.cards.push(card);
    return card;
  };

  CardService.prototype.update = function (card) {
    var index = this.items.findIndex(function(item) {
      return item.id === card.id;
    });
    this.cards[index] = card;
  };

  CardService.prototype.save = function (card) {
    if (card.id) {
      return this.update(card);
    }
    return this.add(card);
  };






  function CardFactory() {

  }

})(angular)
