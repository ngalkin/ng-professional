(function(ng) {

  ng.module('app')
    .directive('cardList', CardListDirective);

  function CardListController(CardService) {
    this.CardService = CardService;
    this.items = CardService.list();
  }

  CardListController.prototype.remove = function (card) {
    this.CardService.remove(card);
    this.items = this.CardService.list();
  };

  function CardListDirective() {
    return {
      restrict: 'E',
      controller: CardListController,
      controllerAs: '$ctrl',
      templateUrl: 'js/card-list/template.html',
      bindToController: true
    }
  }
})(angular)
